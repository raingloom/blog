MKSHELL=$PLAN9/bin/rc

entries/%.html: entries/%.md head.html tail.html
	{cat head.html
	markdown entries/$stem.md
	cat tail.html}>$target 

all-entries:
	for (e in entries/*.md)
		mk $MKFLAGS `{echo $e | sed 's/md$/html/'}

main.html: main.md
	markdown $prereq > $target

index.html:V: all-entries main.html head.html tail.html
	ifs='
	'
	flag x +
	{cat head.html
	cat main.html
	ls entries | grep '\.html$' | sort -r | while (e=`{read}) {
		echo '<p>'
		echo '<h3>'
		echo '<a href=' ^ $e ^ '>'
		# extract title from first line
		sed 1q `{echo $e | sed 's/html$/md/'} | grep '^#' | sed 's/^# //'
		echo '</a>'
		echo '</h3>'
		echo 'Created: '
		git log -n 1 --diff-filter'=A' --pretty'=format:%at' -- $e
		echo ' Last modified:'
		git log -n 1 --pretty'=format:%at' -- $e
		echo ' Word count:'
		wc -w $e | awk '{print($1)}'
		echo '</p>'
	}
	cat tail.html} > $target

nuke:V:
	rm -f index.html entries/*.html

all:V: index.html

publish:V: all
	git add -A
	ifs='
	'
	git commit -m 'publish ' ^ `{date}
	git push origin master
