# ORCA + Sunvox MIDI Setup On Arch Linux For Absolute Noobs (like me)
TLDR: jump to the Guide section

I wanted to try [ORCΛ](https://github.com/hundredrabbits/Orca-c) (the C port of [this](https://github.com/hundredrabbits/Orca)) and Devine suggested [Sunvox](http://www.warmplace.ru/soft/sunvox/) as a MIDI synth, but I had some trouble getting started with it because I have pretty much zero knowledge of MIDI or just digital music in general, so if you are also lost, here is how you do it. ~~this section censored for being embarrassing but basically thanks Devine!!!!~~

## Guide
1. Install portmidi from the AUR:

```
$ yay -S portmidi #or use your preferred AUR helper or do it manually, whatever floats your boat
```

2. Download and build Orca-c
```
$ #step 2/0: have the base-devel package group installed (pacman -S base-devel)
$ cd $to_some_directory_where_you_want_to_clone_the_project
$ git clone https://github.com/hundredrabbits/Orca-c
$ cd Orca-c
$ ./tool --portmidi build release orca
```

3. Start Sunvox
Just... start it normally. From dmenu in my case. Don't modify anything yet, I have no idea what all that beepboop stuff does.

4. Enter Sunvox's settings and set some things
 - Click the weird icon thingy in the top-left corner
 - Select "Preferences"
 - In the window that popped up, click MIDI
 - Click "MIDI Controller 1" and choose a port, in my case it was called "Midi through port-0"
 - And finally, click "Ignore velocity" (it should be on)

5. Test it with an example
Run orca with the _midi.orca example.

## Epilogue
Aaand that's it. I'm not sure "Ignore velocity" is _actually_ necessary if you use the right MIDI settings in Orca, but it apparently didn't hurt to have it on, otherwise I could see the keyboard being pressed in Sunvox, but there was no sound. Now go and play around with Orca, I know I'll be doing that.
